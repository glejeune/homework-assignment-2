const fsStore = require('../utils/fs-store');
const helpers = require('../utils/helpers');
const token = require('./token');
const order = require('./order');

module.exports = {
  findByTokenId(tokenId, callback) {
    token.findByID(tokenId, (err, tokenObject) => {
      if (!err && tokenObject) {
        this.findByEmail(tokenObject.email, callback);
      } else {
        callback({error: 'Invalid token'}, undefined);
      }
    });
  },

  findByEmail(email, callback) {
    fsStore.read('users', email, callback);
  },

  login(email, password, callback) {
    this.findByEmail(email, (err, userObject) => {
      if (!err && userObject) {
        const hashedPassword = helpers.hash(password);
        if (hashedPassword === userObject.hashedPassword) {
          callback(false, userObject);
        } else {
          callback({error: 'Invalid password'}, undefined);
        }
      } else {
        callback({error: 'Invalid user'}, undefined);
      }
    });
  },

  create(user, callback) {
    this.findByEmail(user.email, (err, _userObject) => {
      if (err) {
        const hashedPassword = helpers.hash(user.password);
        if (hashedPassword) {
          const userObject = {
            fullName: user.fullName,
            email: user.email,
            address: user.address,
            hashedPassword,
          };
          fsStore.create('users', user.email, userObject, err => {
            if (!err) {
              delete userObject.hashedPassword;
              callback(false, userObject);
            } else {
              callback({error: 'Failed to create new user'}, undefined);
            }
          });
        } else {
          callback({error: "Could not hash user's password"}, undefined);
        }
      } else {
        callback({error: 'A user with that email already exists'}, undefined);
      }
    });
  },

  update(email, userObject, callback) {
    fsStore.update('users', email, userObject, callback);
  },

  delete(email, callback) {
    // TODO: delete user orders
    this.findByEmail(email, (err, userObject) => {
      if (!err && userObject) {

        const userTokens =
          typeof userObject.tokens === 'object' &&
          userObject.tokens instanceof Array
            ? userObject.tokens
            : [];
        const tokensDeletePromises = userTokens.map(
          currentTokenId =>
            new Promise((resolve, reject) => {
              token.delete(currentTokenId, err => {
                if (!err) {
                  resolve(currentTokenId);
                } else {
                  reject(currentTokenId);
                }
              });
            }),
        );

        const userOrders =
          typeof userObject.orders === 'object' &&
          userObject.orders instanceof Array
            ? userObject.orders
            : [];
        const ordersDeletePromises = userOrders.map(
          currentOrderId =>
            new Promise((resolve, reject) => {
              order.delete(currentOrderId, err => {
                if (!err) {
                  resolve(currentOrderId);
                } else {
                  reject(currentOrderId);
                }
              });
            }),
        );

        Promise.all([...tokensDeletePromises, ...ordersDeletePromises])
          .then(_ => {
            fsStore.delete('users', email, callback);
          })
          .catch(values => {
            callback({
              error: `Could not delete user tokens or orders: ${values.join(', ')}`,
            });
          });
      } else {
        callback({error: 'Failed to delete user'});
      }
    });
  },

  emptyCard(userObject, callback) {
    if (userObject.card.length === 0) {
      callback(false);
    } else {
      userObject.card = [];
      this.update(userObject.email, userObject, err => {
        if (!err) {
          callback(false);
        } else {
          callback({error: 'Failed to delete card'});
        }
      });
    }
  },
};
