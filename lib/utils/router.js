const path = require('path');
const fs = require('fs');
const url = require('url');
const StringDecoder = require('string_decoder').StringDecoder;
const helpers = require('./helpers');
const logger = require('./logger');

const loadControllers = pathname => {
  const stat = fs.lstatSync(pathname);
  let controllers = {};
  if (stat.isDirectory()) {
    const files = fs.readdirSync(pathname);
    const len = files.length;
    for (let i = 0; i < len; i++) {
      const f = path.join(pathname, files[i]);
      controllers = Object.assign(controllers, loadControllers(f));
    }
  } else {
    logger.debug(
      'load controller %s',
      path.basename(pathname, path.extname(pathname)),
    );
    const controller = require(pathname);
    controllers = controller;
  }
  return controllers;
};
const _controllers = loadControllers(path.join(__dirname, '..', 'controllers'));

if (!_controllers.notFound) {
  _controllers.notFound = (data, callback) => {
    callback(404);
  };
}

module.exports = (req, res) => {
  // Path
  const parsedUrl = url.parse(req.url, true);
  const path = parsedUrl.pathname.replace(/^\/+|\/+$/g, '');

  // Query string
  const queryStringObject = parsedUrl.query;

  // HTTP method
  const method = req.method.toLowerCase();

  // Headers
  const headers = req.headers;

  // Body decoder
  const decoder = new StringDecoder('utf-8');
  let payload = '';
  req.on('data', data => {
    payload += decoder.write(data);
  });
  req.on('end', () => {
    payload += decoder.end();

    // Get handler for request
    const _chosenController =
      typeof _controllers[path] !== 'undefined'
        ? _controllers[path]
        : _controllers.notFound;

    // Data object passed to the handler
    const data = {
      path,
      queryStringObject,
      method,
      headers,
      payload: helpers.jsonToObject(payload),
    };

    // Call handler
    _chosenController(data, (statusCode, payload) => {
      statusCode = typeof statusCode === 'number' ? statusCode : 500;
      let responsePayload = undefined;
      if (payload) {
        switch (payload.constructor.name) {
          case 'Object':
            responsePayload = JSON.stringify(payload);
            break;
          case 'Array':
            responsePayload = JSON.stringify(payload);
            break;
          case 'String':
            responsePayload = payload;
            break;
          case 'Number':
            responsePayload = payload.toString();
            break;
          case 'Buffer':
            responsePayload = payload.toString();
            break;
          default:
            undefined;
        }
      }

      res.setHeader('Content-Type', 'application/json');
      res.writeHead(statusCode);
      res.end(responsePayload);
      logger.info('%s /%s - %d', method.toUpperCase(), path, statusCode);
    });
  });
};
