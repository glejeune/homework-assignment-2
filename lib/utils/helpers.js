const fs = require('fs');
const crypto = require('crypto');
const config = require('../config');

module.exports = {
  createIfNotExists(path, callback) {
    if (fs.existsSync(path)) {
      callback(false);
    } else {
      fs.mkdir(path, {recursive: true}, callback);
    }
  },

  syncCloseNoError(fd) {
    try {
      fs.closeSync(fd);
    } catch (_) {}
  },

  jsonToObject(string) {
    try {
      return JSON.parse(string);
    } catch (e) {
      return {};
    }
  },

  hash(string) {
    if (typeof string === 'string' && string.length > 0) {
      const hash = crypto
        .createHmac('sha256', config.hashingSecret)
        .update(string)
        .digest('hex');
      return hash;
    } else {
      return false;
    }
  },

  createRandomString(strLength) {
    if (typeof strLength !== 'number' || strLength <= 0) return false;

    if (strLength) {
      const possibleCharacters = 'abcdefghijklmnopqrstuvwxyz0123456789';
      let str = '';
      for (let i = 1; i <= strLength; i++) {
        str += possibleCharacters.charAt(
          Math.floor(Math.random() * possibleCharacters.length),
        );
      }
      return str;
    } else {
      return false;
    }
  },
};
