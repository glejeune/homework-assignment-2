default: certs
	@node index.js

doc: 
	@rm -f API.md
	@rm -rf doc
	@apidoc -i . -o doc
	@apidoc-markdown2 -p doc -o README.md --prepend _doc/header.md
	@rm -rf doc

clean:
	@rm -rf https .data

certs: https/localhost.crt

https/localhost.crt: https/localhost.csr https/root/root-ca.crt
	@openssl x509 -req -in https/localhost.csr \
		-CA https/root/root-ca.crt \
		-CAkey https/root/root-ca.key \
		-CAcreateserial \
		-out https/localhost.crt -days 500 -sha256

https/localhost.csr: https/localhost.key
	@openssl req -new -sha256 \
		-key https/localhost.key \
		-subj "/C=US/ST=CA/O=$(shell hostname), Inc./CN=localhost" \
		-out https/localhost.csr

https/localhost.key: https
	@openssl genrsa -out https/localhost.key 2048

https/root/root-ca.crt: https/root/root-ca.key
	@openssl req -x509 -new -nodes \
		-key https/root/root-ca.key -sha256 -days 1024 \
		-subj "/C=US/ST=CA/O=glejeune-XPS-13-9343, Inc./CN=localhost" \
		-out https/root/root-ca.crt

https/root/root-ca.key: https/root
	@openssl genrsa -out https/root/root-ca.key 4096

https/root: https
	@mkdir -p https/root

https:
	@mkdir https

ubuntu-install-ca: certs 
	@sudo mkdir -p /usr/local/share/ca-certificates/extra
	@sudo cp https/root/root-ca.crt /usr/local/share/ca-certificates/extra/root-ca.crt
	@sudo update-ca-certificates --fresh

ubuntu-uninstall-ca:
	@sudo rm -f /usr/local/share/ca-certificates/extra/root-ca.crt
	@sudo update-ca-certificates --fresh

ca-auth:
	@awk -v cmd='openssl x509 -noout -subject' ' \
    /BEGIN/{close(cmd)};{print | cmd}' < /etc/ssl/certs/ca-certificates.crt
